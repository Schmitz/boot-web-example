package eu.bbv.springweb.stuff;

import java.util.Arrays;
import java.util.Optional;

public enum SecurityToken {

    USER("qwertz"),
    ADMIN("ztrewq");

    private String token;

    SecurityToken(final String token) {
        this.token = token;
    }

    public Optional<SecurityToken> byTokenString(final String token) {
        return Arrays.stream(SecurityToken.values()).filter(t -> t.getToken().equals(token)).findFirst();
    }

    public String getToken() {
        return token;
    }
}

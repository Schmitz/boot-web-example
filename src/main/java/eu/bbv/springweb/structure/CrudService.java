package eu.bbv.springweb.structure;

public interface CrudService<T> {

    ListResponse<T> list();

    T byId(final String id);

    T save(final T obj);

    void delete(final String id);

    default void test(final String id) {
        System.out.println(id);
    }

}

package eu.bbv.springweb.structure;

import java.util.List;

public class ListResponse<T> {

    // can be extended by attributes for paging (page, pageSize, order, direction, numPages ...)

    protected int size;

    protected List<T> listItems;

    public int getSize() {
        return size;
    }

    public List<T> getListItems() {
        return listItems;
    }
}

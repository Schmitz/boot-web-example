package eu.bbv.springweb.structure;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class AbstractCrudController<T> {

    private final CrudService<T> service;

    protected AbstractCrudController(final CrudService<T> service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<ListResponse<T>> list() {
        return ResponseEntity.ok(this.service.list());
    }

    @GetMapping("/{id}")
    public ResponseEntity<T> byId(final @PathVariable String id) {
        return ResponseEntity.ok(this.service.byId(id));
    }

    @PostMapping
    public ResponseEntity<T> save(final @RequestBody T obj) {
        return ResponseEntity.ok(this.service.save(obj));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(final @PathVariable String id) {
        this.service.delete(id);
        return ResponseEntity.ok().build();
    }


}

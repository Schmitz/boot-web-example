package eu.bbv.springweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableMethodSecurity
public class HttpSecurityConfig {

    // urls without authentication
    private static final String[] AUTH_WHITELIST = {
        "/v3/api-docs/**",
        "/v3/api-docs.yaml",
        "/swagger-ui/**",
        "/swagger-ui.html"
    };

    // TODO authManager || oAuth2 impl
    @Bean
    public SecurityFilterChain securityConfig(HttpSecurity http) throws Exception {
        //@formatter:off
        http
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        .and()
            .authorizeHttpRequests()
            .requestMatchers(AUTH_WHITELIST)
            .permitAll()
            .anyRequest()
            .authenticated()

        .and()
            .cors();

        //@formatter:on
        return http.build();
    }

    @Bean
    @Profile({
        "dev",
        "test"
    })
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring()
            .anyRequest();
    }


}

package eu.bbv.springweb.meal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Meal {

    private String id;
    private String name;
    private float price;
    private String description;

    public Meal(MealEntity mealEntity) {
        this.id = mealEntity.getId();
        this.name = mealEntity.getName();
        this.price = mealEntity.getPrice();
        this.description = mealEntity.getDescription();
    }

}

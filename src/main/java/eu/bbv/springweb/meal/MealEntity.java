package eu.bbv.springweb.meal;


import eu.bbv.springweb.structure.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "meal")
@NoArgsConstructor
@Getter
@Setter
public class MealEntity extends BaseEntity {

    private String name;
    private float price;
    private String description;
    public MealEntity(final Meal meal) {
        this.setId(meal.getId());
        this.name = meal.getName();
        this.price = meal.getPrice();
        this.description = meal.getDescription();
    }

}

package eu.bbv.springweb.meal;

import org.springframework.stereotype.Repository;

import eu.bbv.springweb.structure.PagingCrudJpaRepository;

@Repository
public interface MealPagingCrudJpaRepository extends PagingCrudJpaRepository<MealEntity, String> {
}

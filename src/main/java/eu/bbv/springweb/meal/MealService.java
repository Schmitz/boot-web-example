package eu.bbv.springweb.meal;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.bbv.springweb.structure.CrudService;
import eu.bbv.springweb.structure.ListResponse;

@Service
@Transactional
public class MealService implements CrudService<Meal> {

    private final MealPagingCrudJpaRepository repository;

    public MealService(final MealPagingCrudJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public ListResponse<Meal> list() {
        List<MealEntity> mealEntityList = (List<MealEntity>) this.repository.findAll();
        List<Meal> dtoList = mealEntityList.stream().map(e -> new Meal(e)).collect(Collectors.toList());
        return new MealList(dtoList);
    }

    @Override
    public Meal byId(final String id) {
        Optional<MealEntity> mealEntity = this.repository.findById(id);
        return mealEntity.map(e -> new Meal(e)).orElseThrow();
    }

    @Override
    public Meal save(final Meal obj) {
        MealEntity mealEntity = this.repository.save(new MealEntity(obj));
        return new Meal(mealEntity);
    }

    @Override
    public void delete(final String id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            // custom exception with state
        }
    }
}

package eu.bbv.springweb.meal;

import java.util.List;

import eu.bbv.springweb.structure.ListResponse;

public class MealList extends ListResponse<Meal> {

    public MealList(final List<Meal> meals) {
        this.size = meals.size();
        this.listItems = meals;
    }

}

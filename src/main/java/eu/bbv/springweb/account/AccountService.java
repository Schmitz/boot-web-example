package eu.bbv.springweb.account;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import eu.bbv.springweb.structure.CrudService;
import eu.bbv.springweb.structure.ListResponse;

@Service
public class AccountService implements CrudService<Account> {

    private final AccountPagingCrudJpaRepository repository;

    public AccountService(final AccountPagingCrudJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public ListResponse<Account> list() {
        List<AccountEntity> entityList = (List<AccountEntity>) this.repository.findAll();
        List<Account> accounts = entityList.stream().map(a -> new Account(a)).collect(Collectors.toList());
        return new AccountList(accounts);
    }

    @Override
    public Account byId(final String id) {
        Optional<AccountEntity> account = this.repository.findById(id);
        return account.map(a -> new Account(a)).orElseThrow();
    }

    @Override
    public Account save(final Account obj) {
        AccountEntity account = this.repository.save(new AccountEntity(obj));
        return new Account(account);
    }

    @Override
    public void delete(final String id) {
        try {
            this.repository.deleteById(id);
        } catch (Exception e) {
            // custom exception with state
        }
    }

}

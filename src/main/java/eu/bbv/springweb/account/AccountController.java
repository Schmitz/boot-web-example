package eu.bbv.springweb.account;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.bbv.springweb.structure.AbstractCrudController;
import eu.bbv.springweb.structure.ListResponse;

@RestController
@RequestMapping("/accounts")
public class AccountController extends AbstractCrudController<Account> {

    private final AccountService service;

    public AccountController(final AccountService service) {
        super(service);
        this.service = service;
    }

    @Override
    @Secured("ROLE_ADMIN") // example to override a crud method with custom settings
    public ResponseEntity<ListResponse<Account>> list() {
        return super.list();
    }
}

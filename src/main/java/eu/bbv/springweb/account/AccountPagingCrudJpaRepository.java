package eu.bbv.springweb.account;

import org.springframework.stereotype.Repository;

import eu.bbv.springweb.structure.PagingCrudJpaRepository;

@Repository
public interface AccountPagingCrudJpaRepository extends PagingCrudJpaRepository<AccountEntity, String> {
}

package eu.bbv.springweb.account;

import java.util.List;

import eu.bbv.springweb.structure.ListResponse;

public class AccountList extends ListResponse<Account> {

    public AccountList(final List<Account> accounts) {
        this.size = accounts.size();
        this.listItems = accounts;
    }

}

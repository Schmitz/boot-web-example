package eu.bbv.springweb.account;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class Account {

    private String id;
    private @NonNull String userName;
    private @NonNull String password;
    private String accessToken;

    public Account(final AccountEntity account) {
        this.id = account.getId();
        this.userName = account.getUserName();
        this.accessToken = account.getAccessToken();
        this.password = account.getPassword();
    }
}

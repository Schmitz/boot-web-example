package eu.bbv.springweb.account;

import eu.bbv.springweb.structure.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Table(name = "account")
@NoArgsConstructor
@Getter
@Setter
public class AccountEntity extends BaseEntity {

    private @NonNull String userName;
    private @NonNull String password;
    private String accessToken;

    public AccountEntity(final Account account) {
        this.setId(account.getId());
        this.userName = account.getUserName();
        this.password = account.getPassword();
        this.accessToken = account.getAccessToken();
    }

}

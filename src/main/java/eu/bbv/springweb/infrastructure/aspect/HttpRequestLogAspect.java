package eu.bbv.springweb.infrastructure.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class HttpRequestLogAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestLogAspect.class);

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    private void isRestController() {}


    @After("isRestController()")
    public void logRequest(JoinPoint joinPoint) {
        LOGGER.info(joinPoint.toLongString());
    }


}
